# DemNetd

Decentralized Version of a fully democratic social network based on OrbitDB.

## Architecture
The Social Network is a blogging service that is administered, moderated and developed
democratically by the Members of the Network.

### Administration
The network elects a member to be their Administrator.
The Administrator must ensure the availability of the member's
data, their access to the network and enforce the rules of the
network.

They also approve membership applications.

### Moderation
The members of the network decide about the rules of
the network in elections.

They can create, remove and update rules by election.

The members elects a their Moderators.

Moderators decide about reports of violations of the rules.
An accused member can appeal a decision by a moderator twice.

A decision can be:
1. Nothing. -> Report was not justified.
2. Remove content of the accuser.
3. Ban the accuser from the network.

The third decision only becomes final after
either 2 months or the two appeals affirming the
decision.

The rules of the network can specify what decision must be taken
for violating a rule or they can leave it to the moderator to make a
decision.

When an accused appeals a decision another moderator is assigned to
either concur with the previous decision or overwrite it.

### Development
The source code of the network is free and open source software.
In order for an update to the source code to be deployed to the
network it has to win an election of the members.

### Glossary
- Governance of the Network: This consists of three subsections:
  1. Moderation: Dealing with conflicts.
  2. Administration: Keeping the network operational.
  3. Development: Continuous improvement of the network.
- democratic governance: All three aspects of governance are either directly or indirectly legitimized by an election among the members of the network.
- Members of a Network: People with Voting Rights and using the network.
- Join a Network: Applying for Membership in a network.
- Leave a Network: Disconituing the Membership in a network.
- OrbitDB: CRDT based distributed Database built on IPFS.
