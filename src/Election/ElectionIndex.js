"use strict"
/**
 * The Index for Election does not validate the authenticity of votes.
 * See AccessController.js
 * This Index implements Score Voting with 0 meaning abstention and the upper
 * limit being determined by the db manifest.
 */

module.exports = (meta) => {
  return class Index {
    constructor() {
      this.clear()
    }

    clear() {
      this._proposals = []
      this._votes = {}
      this._voters = meta.voters
      this._limit = meta.limit // upper limit of the scoring.
    }

    get proposals() {
      return this._proposals
    }

    get votes() {
      return this._votes
    }

    get voters() {
      return this._voters
    }

    get limit() {
      return this._limit
    }

    get winner() {

      if (Object.keys(this._votes).length != this._voters.length) {
        return null
      }

      let scores = {}
      let winner = null;
      for (let proposal of this._proposals) {
        scores[proposal] = 0
        for (let vote of Object.values(this._votes)) {
          scores[proposal] += vote[proposal] % this._limit
        }

        scores[proposal] /= Object.keys(this._votes).length

        if (scores[proposal] > scores[winner] || winner == null) {
          winner = proposal
        }
      }
      return winner
    }

    propose(option) {
      this._proposals.push(option)
    }

    vote(identity, vote) {
      this._votes[identity] = vote
    }

    /**
     * Votes can be overwritten - until an absolute majority of voters
     * has voted.
     */
    updateIndex(oplog) {
      this.clear()
      let values = oplog.values.slice()

      for (let operation of values) {

        if (this.winner) {
          break
        } else {
          switch (operation.payload.op) {
            case "PROPOSE":
              this.propose(operation.payload.option)
              break;
            case "VOTE":
              this.vote(operation.identity.id, operation.payload.vote)
              break
            default:
              break
          }
        }
      }
    }
  }
}