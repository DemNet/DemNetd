"use strict"
/**
 * The Store for a score vote election.
 * The set of voters is immutable after the
 * election is created
 */
const ElectionIndex = require("./ElectionIndex")
const Store = require("orbit-db-store")

class ElectionStore extends Store {
  constructor(ipfs, id, dbname, options) {
    if (!options) options = {}
    if (!options.Index) Object.assign(options, {
      Index: ElectionIndex(options.meta)
    })

    if (!options.accessController) {
      options.accessController = {
        type: "ipfs",
        write: options.meta.voters,
      }
    }

    super(ipfs, id, dbname, options)
    this._type = ElectionStore.type
  }

  static get type() {
    return "electionstore"
  }

  get voters() {
    return this._index._voters
  }

  get proposals() {
    return this._index._proposals
  }

  get votes() {
    return this._index._votes
  }

  get winner() {
    return this._index.winner
  }

  propose(option) {
    return this._addOperation({
      op: "PROPOSE",
      option: option,
    })
  }

  vote(vote) {
    return this._addOperation({
      op: "VOTE",
      vote: vote,
    })
  }
}

module.exports = ElectionStore