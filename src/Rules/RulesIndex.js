"use strict"
/**
 * Store of the rules ratified by an election
 */
const ElectionIndex = require("../Election/ElectionIndex")

module.exports = (ipfs, meta) => {
  class RulesIndex {
    constructor() {
      this.clear()
    }

    clear() {
      this.rules = {}

      this.members = meta.members
      this.admins = meta.admins
      this.mods = meta.mods

      this.elections = {}
      this.INDEX = ElectionIndex({
        voters: this.members,
        limit: meta.limit
      })
    }


    addRule(id, rule, election) {
      this.rules[id] = {
        rule: rule,
        election: election,
      }
    }

    addMember(id) {
      this.members.push(id)
      this.INDEX = ElectionIndex({
        voters: this.members,
        limit: meta.limit
      })
    }

    addAdmin(id) {
      this.admins.push(id)
    }

    addMods(id) {
      this.mods.push(id)
    }

    propose({
      candidacies: [],
      rules: []
    }) {
      let proposal = {
        candidacies: candidacies,
        rules: rules
      }

      let cid = await ipfs.dag.put(proposal)
      this.elections[cid] = new this.INDEX()
    }

    vote(cid, identity, vote) {
      if (this.elections[cid].winner) {
        return
      }

      this.elections[cid].vote(identity, vote)

      if (this.elections[cid].winner) {
        this.apply(cid, this.elections[cid])
      }
    }

    apply(proposalCID, election) {
      let proposal = await ipfs.dag.get(proposalCID)

      for (let candidate of proposal.candidacies) {
        switch (candidate.type) {
          case "REMOVE":
            delete this[candidate.role].delete(candidate.id)
          case "APPOINT":
            this[candidate.role].add(candidate.id)
        }
      }

      for (let rule of proposal.rules) {
        this.rules[rule.id] = rule
      }
    }

    updateIndex(oplog) {
      let values = oplog.values.slice()

      for (let operation of values) {
        switch (operation.payload.op) {
          case "PROPOSE":
            this.propose({
              candidacies: operation.payload.candidacies.filter(c => ["admins", "mods"].indexOf(c.role) != -1),
              rules: operation.payload.rules,
            })
            break;
          case "VOTE":
            this.vote(operation.payload.cid, operation.identity.id, operation.payload.vote)
            break
          case "ADDMEMBER":
            if (this.admins.has(operation.identity.id)) {
              this.addMember(operation.payload.id)
            }
            break;

          case ""

          default:

        }
      }
    }
  }
}