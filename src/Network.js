"use strict"

const OrbitDBStore = require("orbit-db-store")
const NetworkIndex = require("./NetworkIndex")

class Network extends Store {
  constructor(ipfs, id, dbname, options) {
    if (!options) options = {}
    if (!options.Index) Object.assign(options, {
      Index: NetworkIndex
    })

    super(ipfs, id, dbname, options)
    this._type = "network"
  }

  get moderators() {
    return this._index.moderators
  }

  get admins() {
    return this._index.admins
  }

  get members() {
    return this._index.members
  }

  get rules() {
    return this._index.rules
  }

  // VOTING
  propose(proposal) {
    return this._addOperation({
      op: "PROPOSE",
      proposal: proposal,
    })
  }

  vote(proposalID) {
    return this._addOperation({
      op: "VOTE",
      proposalID: proposalID
    })
  }

  // CRUD of posts

  createPost(post) {
    return this._addOperation({
      op: "CREATEPOST",
      post: post
    })
  }

  get posts() {
    return this._index.posts
  }

  postsOf(identity) {
    return this._index.postsOf(identity)
  }

  updatePost(postID, changes) {
    return this._addOperation({
      op: "UPDATEPOST",
      postID: postID,
      changes: changes
    })
  }

  removePost(postID) {
    return this._addOperation({
      op: "REMOVEPOST",
      postID: postID
    })
  }

  // MODERATION
  report(report) {
    return this._addOperation({
      op: "REPORT",
      report: report,
    })
  }

  decision(reportID, decision) {
    return this._addOperation({
      op: "DECISION",
      reportID: reportID,
      decision: decision,
    })
  }

  appeal(decisionID) {
    return this._addOperation({
      op: "APPEAL",
      decisionID: decisionID,
    })
  }
}