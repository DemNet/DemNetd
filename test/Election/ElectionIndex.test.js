const Index = require("../../src/Election/ElectionIndex")

let index
const voters = ["a", "b", "c", "d"]
const limit = 9

test("Create an Index", () => {
  const INDEX = Index({
    voters: voters,
    limit: limit,
  })

  index = new INDEX()

  expect(index._voters).toBe(voters)
  expect(index._limit).toBe(limit)
})

test("Create proposal", () => {
  index.propose("A")
  index.propose("B")
  index.propose("C")

  expect(index._proposals).toContain("A")
  expect(index._proposals).toContain("B")
  expect(index._proposals).toContain("C")
  expect(index._proposals).not.toContain("D")
})

test("No winner if not all voters have voted.", () => {
  expect(index.winner).toBe(null)
})

test("Creates votes", () => {
  index.vote("a", {
    "A": 9,
    "B": 2,
    "C": 5,
  })
  index.vote("b", {
    "A": 4,
    "B": 9,
    "C": 6,
  })
  index.vote("c", {
    "A": 2,
    "B": 5,
    "C": 17, // mod 9 = 8
  })

  expect(Object.keys(index._votes)).toContain("a")
  expect(Object.keys(index._votes)).toContain("b")
  expect(Object.keys(index._votes)).toContain("c")
  expect(Object.keys(index._votes)).not.toContain("d")
})

test("Winnig the election", () => {
  index.vote("d", {
    "A": 2,
    "B": 2,
    "C": 9,
  })

  expect(index.winner).toBe("C")
})