const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")
const Identities = require('orbit-db-identity-provider')
const ElectionStore = require("../../src/Election/ElectionStore")

let ipfs
let identity
let orbitdb
let election

OrbitDB.addDatabaseType(ElectionStore.type, ElectionStore)

beforeAll(async () => {
  ipfs = await IPFS.create({
    offline: true,
    silent: true,
  })

  identity = await Identities.createIdentity({
    id: "local-id",
    repository: ".jsipfs" + Math.random(),
  })

  orbitdb = await OrbitDB.createInstance(ipfs, {
    identity: identity,
    directory: "orbitdb" + Math.random()
  })
})

test("Create new election", async () => {
  election = await orbitdb.create("testElection", ElectionStore.type, {
    meta: {
      voters: [identity.id],
      limit: 9,
    }
  })

  expect(election.voters).toStrictEqual([identity.id])
})

test("Make proposals", async () => {
  await election.propose("A")
  await election.propose("B")
  await election.propose("C")

  expect(election.proposals).toStrictEqual(["A", "B", "C"])
})

test("No winner yet", () => {
  expect(election.winner).toBe(null)
})

test("Vote", async () => {
  expect(Object.keys(election.votes)).toStrictEqual([])

  let vote = {
    "A": 3,
    "B": 2,
    "C": 9,
  }

  await election.vote(vote)

  expect(Object.keys(election.votes)).toStrictEqual([identity.id])
  expect(election.votes[identity.id]).toBe(vote)
})

test("Winner is A", () => {
  expect(election.winner).toBe("A")
})